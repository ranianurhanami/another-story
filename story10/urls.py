from django.urls import path, include
from .views import login_page, register,logout_page, welcome

app_name = "story10"

urlpatterns = [
    path('', login_page, name='login'),
    path('register/', register, name='register'),
    path('logout/', logout_page, name='logout'),
    path('welcome/', welcome, name='welcome'),
]
