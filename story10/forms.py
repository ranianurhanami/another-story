from django import forms

class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(
        attrs={
            'class' : 'form-control',
            'type' : 'text',
            'required' : True,
            'autocomplete' : 'off',
        }
    ))
    password = forms.CharField(widget=forms.TextInput(
        attrs={
            'class' : 'form-control',
            'type' : 'password',
            'required' : True,
            'autocomplete' : 'off',
        }
    ))


class RegisterForm(forms.Form):
    email = forms.CharField(widget=forms.TextInput(
        attrs={
            'class' : 'form-control',
            'type' : 'email',
            'required' : True,
            'autocomplete' : 'off',
        }
    ))
    username = forms.CharField(widget=forms.TextInput(
        attrs={
            'class' : 'form-control',
            'type' : 'text',
            'required' : True,
            'autocomplete' : 'off',
        }
    ))

    password = forms.CharField(widget=forms.TextInput(
        attrs={
            'class' : 'form-control',
            'type' : 'password',
            'required' : True,
            'autocomplete' : 'off',
        }
    ))
