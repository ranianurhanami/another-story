from django.shortcuts import render, redirect, HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

from .forms import LoginForm, RegisterForm

# Create your views here
def login_page(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            input_username = form.cleaned_data["username"]
            input_password = form.cleaned_data["password"]
            user = authenticate(request, username = input_username, password = input_password)
            if user is not None:
                login(request, user)
                return redirect('story10:welcome')
    form = LoginForm()
    response = {
        "form" : form,        
    }
    return render(request, "login.html", response)

@login_required(login_url='/')
def logout_page(request):
    logout(request)
    return redirect("story10:login")

def register(request):
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if (form.is_valid()):
            input_email = form.cleaned_data["email"]
            input_username = form.cleaned_data["username"]
            input_password = form.cleaned_data["password"]
            try:
                user_temp = User.objects.get(username = input_username)
                return redirect("story10:login")
            except User.DoesNotExist:
                user_temp = User.objects.create_user(input_username, input_email, input_password)
                return redirect("story10:login")
    form = RegisterForm()
    response = {
        "form" : form
    }
    return render(request, "register.html", response)

@login_required
def welcome(request):
    return render(request, 'welcome.html')
