from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.urls import resolve, reverse
from .views import welcome,register, logout

from . import views

class UrlsTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_url_login_ada(self):
        response = self.client.get("")
        self.assertEqual(response.status_code,200)
    
class ViewsTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user('rania@gmail.com', 'rania', 'raniahanami')
        self.user.save()

    def test_login(self):
        response = self.client.post('', data={'username' : 'rania', 'password' : 'raniahanami'})
        self.assertEqual(response.status_code,200)
      
    def test_register(self):
        user = User(username='raniah', password='rania')
        user.save()
        jumlah = User.objects.filter(username='raniah').count()
        self.assertEqual(jumlah, 1)
    
    def test_welcome(self) :
        response = resolve("/welcome/")
        self.assertEqual(response.func, welcome)

        response = resolve("/register/")
        self.assertEqual(response.func, register)

    def test_register_udah_Ada(self):
        response = self.client.post('/register/', data={'username' : 'rania', 'password' : 'raniahanami'})
        self.assertEqual(response.status_code,200)

class TemplateTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.logout = reverse('story10:logout')
        self.register = reverse('story10:register')

    def test_html_login(self):
        response = self.client.get("")
        self.assertTemplateUsed(response,'login.html')
    