from django.test import TestCase, Client
from django.urls import reverse, resolve;
from .views import funfact
from django.apps import apps
from .apps import Story7Config

# Create your tests here.
class TestFunfact(TestCase):
    def test_apakah_url_funfact_ada(self):
                response = Client().get('')
                self.assertEquals(response.status_code, 200)

    # def test_apakah_di_halaman_funfact_ada_templatenya(self):
    #             response = Client().get('')
    #             self.assertTemplateUsed(response, 'funfact.html')

    def test_apakah_funfact_menggunakan_fungsi_yang_benar(self):
                self.funfact = reverse("story7:funfact")
                found = resolve(self.funfact)
                self.assertEqual(found.func, funfact)

    def test_apps(self):
                self.assertEqual(Story7Config.name,'story7')
                self.assertEqual(apps.get_app_config('story7').name,'story7')


                
