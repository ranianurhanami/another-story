$(document).ready(function(){
    $("#user-input").keyup(function(){
        var input_user = $("#user-input").val()
        var res = ""

        $.ajax({
            url :  "data?q=" + input_user,
            error: function(){
                $('#result').empty();
            },
            success: function(data){ 
                // console.log(data.items)               
                $('#result').empty();
                for(var counter = 0; counter < data.items.length; counter++){
                    res += '<tr>';
                    if(jQuery.type(data.items[counter].volumeInfo.imageLinks) === 'undefined'){
                        res += "<td> No Image </td>";  
                    } else {
                        res += '<td>' + '<img src=' + data.items[counter].volumeInfo.imageLinks.thumbnail + "> </td>";
                    }
                    res += '<td>' + data.items[counter].volumeInfo.title + '</td>';
                    
                    res += '<td>';
                    if(jQuery.type(data.items[counter].volumeInfo.authors) === 'undefined'){
                        res += '-'
                    } else{
                        if(data.items[counter].volumeInfo.authors.length == 1){
                            res += data.items[counter].volumeInfo.authors[0]
                        } else{
                            res += '<ul>';
                            for(var j = 0; j < data.items[counter].volumeInfo.authors.length; j++){
                                res += '<li>' + data.items[counter].volumeInfo.authors[j] + '</li>';
                            }
                            res += '</ul>';
                        }
                    }
                    res += '</td>'
                    if(jQuery.type(data.items[counter].volumeInfo.publisher) === 'undefined'){
                        res += "<td> - </td>";   
                    } else{
                        res += '<td>' + data.items[counter].volumeInfo.publisher + '</td>';
                    }

                    if(jQuery.type(data.items[counter].volumeInfo.previewLink) === 'undefined'){
                        res += "<td> - </td>";   
                    } else{
                        res += '<td> <a href="' + data.items[counter].volumeInfo.previewLink + '" target="_blank"> here </a> </td>';
                    }

                    

                    res += '</tr>'
                }
                $("#result").append(res)
            },

        })
    })
})
