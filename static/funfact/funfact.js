var acc = document.getElementsByClassName("accordion");
var down = document.getElementsByClassName("down");
var up = document.getElementsByClassName("up");
var i;

function swapElements(obj1, obj2) {
  // create marker element and insert it where obj1 is
  var temp = document.createElement("div");
  obj1.parentNode.insertBefore(temp, obj1);

  // move obj1 to right before obj2
  obj2.parentNode.insertBefore(obj1, obj2);

  // move obj2 to right before where obj1 used to be
  temp.parentNode.insertBefore(obj2, temp);

  // remove temporary node
  temp.parentNode.removeChild(temp);
}


// Accordion show and hide
for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
// Button down pushed
for (i = 0; i < down.length; i++) {
  var c = acc[i];
  down[i].addEventListener("click", function(){
    var panel = this.parentNode;
    var panel2 = panel.nextElementSibling.nextElementSibling;
    var content1 = panel.nextElementSibling;
    var content2 = panel2.nextElementSibling;
    swapElements(panel,panel2)
    swapElements(content1,content2)
  })
}

// Button up pushed
for (i = 0; i < up.length; i++) {
  var c = acc[i];
  up[i].addEventListener("click", function(){
    var panel = this.parentNode;
    var panel2 = panel.previousElementSibling.previousElementSibling;
    var content1 = this.parentNode.nextElementSibling;
    var content2 = panel.previousElementSibling;
    swapElements(panel2,panel)
    swapElements(content2,content1)
  })
}
