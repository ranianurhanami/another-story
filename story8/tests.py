from django.test import TestCase, Client
from django.urls import reverse, resolve;
from .views import books, fungsi_data
from django.apps import apps
from .apps import Story8Config

# Create your tests here.
class TestBooks(TestCase):
    def test_apakah_url_books_ada(self):
                response = Client().get('/books/')
                self.assertEquals(response.status_code, 302)

    # def test_apakah_di_halaman_books_ada_templatenya(self):
    #             response = Client().get('/books/')
    #             self.assertTemplateUsed(response, 'books.html')

    def test_apakah_books_menggunakan_fungsi_yang_benar(self):
                self.books = reverse("story8:books")
                found = resolve(self.books)
                self.assertEqual(found.func, books)

    def test_apakah_books_list_menggunakan_fungsi_yang_benar(self):
                self.list_book = reverse("story8:fungsi_data")
                found = resolve(self.list_book)
                self.assertEqual(found.func, fungsi_data)
                
    def test_apps(self):
                self.assertEqual(Story8Config.name,'story8')
                self.assertEqual(apps.get_app_config('story8').name,'story8')



                
