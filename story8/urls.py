from django.urls import path, include
from .views import books, fungsi_data

app_name = "story8"

urlpatterns = [
    path('', books, name='books'),
    path('data/', fungsi_data, name = "fungsi_data"),
]
