from django.shortcuts import render, HttpResponse
from django.http import JsonResponse
import urllib.request, json
import requests
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url="/")
def books(request):
    response = {}
    return render(request, 'books.html', response)
@login_required(login_url="/")
def fungsi_data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q="+ request.GET['q']
    res = requests.get(url)
    data = json.loads(res.content)
    return JsonResponse(data)
